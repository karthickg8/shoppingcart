from django.conf.urls import url
from . import views
from django.contrib import admin
#from django.urls import path


admin.site.site_header = "Supermarket"
admin.site.site_title = admin.site.site_header + " Admin Portal"
admin.site.index_title = "Welcome to " + admin.site.site_header + " Portal"
app_name = 'shop'

urlpatterns = [
    url(r'^$', views.product_list, name='product_list'),
    url(r'^(?P<category_slug>[-\w]+)/$', views.product_list, name='product_list_by_category'),
    url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.product_detail, name='product_detail'),
]
#path('logout', views.logout),