from django.shortcuts import render
from .models import OrderItem
from .forms import OrderCreateForm
from cart.cart import Cart
from django.core.mail import send_mail


def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save()
            message = 'Hi ' + request.user.username + ",<br><br>Please find your shopping order details below:<br><br>"
            html_message = "<div class=\"container\"><b>" + message + "</b><table class=\"table table-striped table-hover\" style=\"border-bottom:1px solid #eeeeee;width:100%;min-width:100%\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" bgcolor=\"#ffffff\"><thead style=\"background-color: #FF6347\">"
            html_message += "<tr><th>Name</th><th>Quantity</th><th>Unit Price</th><th>Price</th></tr></thead><tbody align=\"center\">"
            total_cost = 0
            for item in cart:
                ordered_item = OrderItem.objects.create(
                    order=order,
                    product=item['product'],
                    price=item['price'],
                    quantity=item['quantity']
                )
                items_price = ordered_item.quantity * ordered_item.price
                total_cost = total_cost + items_price
                html_message += "<tr><td>" + item['product'].name + "</td><td>" + str(
                    ordered_item.quantity) + "</td><td>INR. " + str(ordered_item.price) + "</td><td>INR. " + str(
                    items_price) + "</td></tr>"
            html_message += "<tr style=\"background-color: #FF6347\"><td><b>Total</b></td><td colspan=\"2\"></td><td colspan=\"num\"><b>INR." + str(
                total_cost) + "</b></td></tr></tbody></table></div>"

            cart.clear()
            subject = 'Supermarket shopping order details'
            send_mail(subject, '', 'startupcompany2050@gmail.com', ['startupcompany2050@gmail.com'], False, None, None, None, html_message)
        return render(request, 'orders/order/created.html', {'order': order})
    else:
        form = OrderCreateForm()
    return render(request, 'orders/order/create.html', {'form': form})